-- phpMyAdmin SQL Dump
-- version 4.0.10.2
-- http://www.phpmyadmin.net
--
-- 主机: localhost
-- 生成日期: 2014-10-24 09:48:44
-- 服务器版本: 5.1.73
-- PHP 版本: 5.3.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- 数据库: `pc_weixin`
--

-- --------------------------------------------------------

--
-- 表的结构 `tbl_activity`
--

DROP TABLE IF EXISTS `tbl_activity`;
CREATE TABLE IF NOT EXISTS `tbl_activity` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(128) NOT NULL,
  `desc` varchar(255) NOT NULL,
  `banner` varchar(255) NOT NULL,
  `award_bg` varchar(255) NOT NULL,
  `keyword` varchar(255) NOT NULL,
  `keyword_pic` varchar(255) NOT NULL,
  `invite_logo` varchar(255) NOT NULL,
  `invite_title` varchar(255) NOT NULL,
  `invite_desc` varchar(255) NOT NULL,
  `rule` varchar(2000) NOT NULL,
  `starttime` int(10) unsigned NOT NULL,
  `endtime` int(10) unsigned NOT NULL,
  `num` int(10) unsigned NOT NULL,
  `status` tinyint(3) unsigned NOT NULL,
  `invite_more` tinyint(1) unsigned NOT NULL,
  `people` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- 转存表中的数据 `tbl_activity`
--

INSERT INTO `tbl_activity` (`id`, `title`, `desc`, `banner`, `award_bg`, `keyword`, `keyword_pic`, `invite_logo`, `invite_title`, `invite_desc`, `rule`, `starttime`, `endtime`, `num`, `status`, `invite_more`, `people`) VALUES
(1, '【转发有礼】恭喜您，信息提交成功！请点击进入抽奖', '转发安博会微邀请函后，按照格式“xx(合作伙伴/用户/员工，选一)、公司名、姓名、手机号”发送信息参与活动', './Uploads/1410852876_iyjULR.png', './Uploads/1413963173_AXerSc.png', '|合作伙伴|用户|', './Uploads/1413857362_VsAPGX.jpeg', './Uploads/1413974872_sAQRJv.jpeg', 'XX豪礼大派送 iPhone 6不等土豪 只等您', '先关注XX科技，按照“xx(合作伙伴/用户/员工，选一)、公司名、姓名、手机号”的格式发送信息参与活动', '1、先关注“XX科技”再抽奖，下拉可查看最新中奖名单↓↓↓；|2、1人有3次抽奖机会，1人只可领取1次礼品；|3、邀请好友参与活动，可以增加中奖概率哦；|4、中奖后正确填写收货信息，礼品将在七个工作日内陆续发放；|5、向XX科技微信发送手机号，即可查询中奖信息及快递信息。', 1414026000, 1414767600, 3, 1, 0, 2000),
(3, '【签到有礼】恭喜您，信息提交成功！请点击进入抽奖', '关注“XX科技”，签到成功，即可抽奖', '', './Uploads/1412844712_FivUnq.png', '|签到|', './Uploads/1413857403_vtuaRn.jpeg', './Uploads/1414068129_oaSXvY.jpeg', 'XX喊您来签到 iPhone 6大派送', '关注“XX科技”，发送信息“安博会”赢取iPhone 6', '1、先关注“XX科技”再抽奖；|2、1人有3次抽奖机会，1人只可领取1次礼品；|3、邀请好友参与活动，可以增加中奖概率哦；|4、中奖后认真正确填写信息：姓名、手机号、地址，礼品将根据填写信息进行发放；|5、向XX科技微信发送收货地址中的手机号，即可查询中奖信息及快递信息。', 1411977336, 1414569336, 3, 1, 0, 10000),
(4, '【趣味问答】恭喜您，闯关成功！请点击进入抽取iPhone 6！', '趣味五连答 好礼送不停！', '', './Uploads/1413861469_EazvrR.png', '|5A|5a|', './Uploads/1413861469_oBPwIN.jpeg', './Uploads/1414068144_YKNtCG.jpeg', '趣味五连答 好礼送不停！', '关注“XX科技”，发送信息“答题”即可进入活动，好礼等您来拿！', '1、先关注“XX科技”再抽奖；|2、1人有3次抽奖机会，1人只可领取1次礼品；|3、邀请好友参与活动，可以增加中奖概率哦；|4、中奖后认真正确填写信息：姓名、手机号、地址，礼品将根据填写信息进行发放；|5、向XX科技微信发送收货地址中的手机号，即可查询中奖信息及快递信息。\r\n', 1413820808, 1414771200, 3, 1, 0, 2000);

-- --------------------------------------------------------

--
-- 表的结构 `tbl_award`
--

DROP TABLE IF EXISTS `tbl_award`;
CREATE TABLE IF NOT EXISTS `tbl_award` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `openid` varchar(64) NOT NULL,
  `actid` int(10) unsigned NOT NULL,
  `giftid` int(11) unsigned NOT NULL,
  `addtime` int(11) NOT NULL,
  `giftlistorder` int(10) unsigned NOT NULL,
  `isAward` tinyint(3) unsigned NOT NULL,
  `isSend` tinyint(3) unsigned NOT NULL,
  `sendtime` int(10) unsigned DEFAULT NULL,
  `express` varchar(32) DEFAULT NULL,
  `expressNo` varchar(64) DEFAULT NULL,
  `sendname` varchar(20) DEFAULT NULL,
  `sendmobile` varchar(20) DEFAULT NULL,
  `sendaddr` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=362 ;

--
-- 表的结构 `tbl_gift`
--

DROP TABLE IF EXISTS `tbl_gift`;
CREATE TABLE IF NOT EXISTS `tbl_gift` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `actid` int(10) unsigned NOT NULL,
  `title` varchar(128) NOT NULL,
  `listorder` int(10) unsigned NOT NULL,
  `isAward` tinyint(3) unsigned NOT NULL,
  `num` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=25 ;

--
-- 转存表中的数据 `tbl_gift`
--

INSERT INTO `tbl_gift` (`id`, `actid`, `title`, `listorder`, `isAward`, `num`) VALUES
(1, 1, 'iPhone 6', 1, 1, 0),
(2, 1, '双肩包', 2, 1, 13),
(3, 1, '500元加油卡', 3, 1, 1),
(4, 1, '谢谢参与', 4, 0, 827),
(5, 1, '高档笔记本', 5, 1, 297),
(6, 1, '旅行4件套', 6, 1, 33),
(7, 1, '500G移动硬盘', 7, 1, 1),
(8, 1, '谢谢参与', 8, 0, 827),
(9, 3, '双肩包', 2, 1, 10),
(10, 3, 'XX笔记本', 3, 1, 200),
(11, 3, '谢谢参与', 4, 0, 9688),
(12, 3, 'XXU盘', 5, 1, 50),
(13, 3, '500元加油卡', 6, 1, 1),
(14, 3, '旅行4件套', 7, 1, 50),
(15, 3, '500G移动硬盘', 8, 1, 1),
(16, 3, 'iPhone6', 1, 1, 0),
(17, 4, 'iPhone 6', 1, 1, 0),
(18, 4, '谢谢参与', 2, 0, 877),
(19, 4, 'XX笔记本', 3, 1, 98),
(20, 4, '500元加油卡', 4, 1, 1),
(21, 4, '旅行4件套', 5, 1, 49),
(22, 4, '谢谢参与', 6, 0, 877),
(23, 4, 'XX笔记本', 7, 1, 97),
(24, 4, '500G移动硬盘', 8, 1, 1);

-- --------------------------------------------------------

--
-- 表的结构 `tbl_invite`
--

DROP TABLE IF EXISTS `tbl_invite`;
CREATE TABLE IF NOT EXISTS `tbl_invite` (
  `openid` varchar(64) NOT NULL,
  `actid` int(10) unsigned NOT NULL,
  `fromopenid` varchar(64) NOT NULL,
  `attention` tinyint(4) NOT NULL,
  `addtime` int(11) NOT NULL,
  `ip` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `tbl_invite`
--

INSERT INTO `tbl_invite` (`openid`, `actid`, `fromopenid`, `attention`, `addtime`, `ip`) VALUES
('0', 3, 'o5F7_jiX5j8GVYq16wtnvdo4Rp9I', 0, 1413878710, '36.16.3.8'),
('0', 3, 'o5F7_jiX5j8GVYq16wtnvdo4Rp9I', 0, 1413880407, '180.153.214.189'),
('0', 1, 'o5F7_jiX5j8GVYq16wtnvdo4Rp9I', 0, 1413963210, '36.16.73.40'),
('0', 1, 'o5F7_jiX5j8GVYq16wtnvdo4Rp9I', 0, 1413963789, '112.64.235.249'),
('0', 1, 'o5F7_jiX5j8GVYq16wtnvdo4Rp9I', 0, 1413969705, '36.16.72.215');

-- --------------------------------------------------------

--
-- 表的结构 `tbl_invite2`
--

DROP TABLE IF EXISTS `tbl_invite2`;
CREATE TABLE IF NOT EXISTS `tbl_invite2` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `openid` varchar(128) NOT NULL,
  `typename` varchar(255) NOT NULL,
  `company` varchar(255) NOT NULL,
  `position` varchar(20) DEFAULT NULL,
  `name` varchar(128) NOT NULL,
  `mobile` varchar(32) NOT NULL,
  `addtime` int(10) unsigned NOT NULL,
  `ip` varchar(24) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `tbl_user`
--

DROP TABLE IF EXISTS `tbl_user`;
CREATE TABLE IF NOT EXISTS `tbl_user` (
  `openid` varchar(64) NOT NULL,
  `name` varchar(20) DEFAULT NULL,
  `mobile` varchar(20) DEFAULT NULL,
  `addr` varchar(128) DEFAULT NULL,
  `lastscope` int(11) DEFAULT NULL,
  `ip` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
