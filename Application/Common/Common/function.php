<?php
/**
 * Created by IntelliJ IDEA.
 * User: appie
 * Date: 14-4-29
 * Time: 下午3:10
 */
require_once 'curl_helper.php';
function logg($content)
{
    file_put_contents("d.txt", "\n".date('Y-m-d H:i:s',time()).':'.$content."\n", FILE_APPEND);
}
/**
 * 根据错误码返回一个错误信息
 * @param $code
 * @param int $type
 * @return mixed
 */
function errorByCode($code, $msg='', $type=0)
{
    $result = C('JSON_RESPONSE_MODEL');
    $result['code'] = $code;
    $result['errorType'] = $type;
    $result['message'] = $msg;
    if($code != 0 && !$msg)
    {
        $result['message'] = errorStringByCode($code, $type);
    }
    
    return $result;
}

function successByData($data)
{
    $result = C('JSON_RESPONSE_MODEL');
    $result['data'] = $data;
    return $result;
}

/**
 * 根据错误码返回具体的错误信息
 * @param $code
 * @return string
 */
function errorStringByCode($code, $type=0)
{
    $error = array();
    switch ($type) 
    {
        // case 0:
        //     $error = require(CONF_PATH.'error.php');
        //     break;
        // case 1:
        //     $error = require(CONF_PATH.'baiduError.php');
        //     break;
        default:
            $error = require(CONF_PATH.'error.php');
            break;
    }
    $msg = 'unknown';
    if(is_array($error) && isset($error[$code]))
    {
        $msg = $error[$code];
    }
    return $msg;
}

function test()
{
    echo 'test';
}

function stringByDeviceStatus($status)
{
    if($status>0)
    {
        return "在线";
    }
    else
    {
        return "离线";
    }
}

/**
+----------------------------------------------------------
 * 产生随机字串，可用来自动生成密码
 * 默认长度6位 字母和数字混合 支持中文
+----------------------------------------------------------
 * @param string $len 长度
 * @param string $type 字串类型
 * 0 字母 1 数字 其它 混合
 * @param string $addChars 额外字符
+----------------------------------------------------------
 * @return string
+----------------------------------------------------------
 */
function rand_string($len = 6, $type = '', $addChars = '') {
    $str = '';
    switch ($type) {
        case 0 :
            $chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz' . $addChars;
            break;
        case 1 :
            $chars = str_repeat ( '0123456789', 3 );
            break;
        case 2 :
            $chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ' . $addChars;
            break;
        case 3 :
            $chars = 'abcdefghijklmnopqrstuvwxyz' . $addChars;
            break;
        default :
            // 默认去掉了容易混淆的字符oOLl和数字01，要添加请使用addChars参数
            $chars = 'ABCDEFGHIJKMNPQRSTUVWXYZabcdefghijkmnpqrstuvwxyz23456789' . $addChars;
            break;
    }
    if ($len > 10) { //位数过长重复字符串一定次数
        $chars = $type == 1 ? str_repeat ( $chars, $len ) : str_repeat ( $chars, 5 );
    }
    if ($type != 4) {
        $chars = str_shuffle ( $chars );
        $str = substr ( $chars, 0, $len );
    } else {
        // 中文随机字
        for($i = 0; $i < $len; $i ++) {
            $str .= msubstr ( $chars, floor ( mt_rand ( 0, mb_strlen ( $chars, 'utf-8' ) - 1 ) ), 1 );
        }
    }
    return $str;
}


/**
* 上传文件 表单必须有属性：ENCTYPE="multipart/form-data"
* @param form_name 文件所在表单的字段名 默认为file1
* @param savepath 文件保存目录 默认为./Uploads/
* @param allowExt 允许上传的扩展性 默认为常用图片
* @param allowSize 允许上传的文件大小 单位 字节 默认为2M 同时也取决php的最大上传文件大小的限制
* @return 返回数组 arr[0]为0表示上传成功 arr[1]存放上传后的文件路径 arr[0]为非0表示上传失败 arr[1]存放错误信息
*/
function upload_file($form_name='file1',$savepath='./Uploads/',$allowExt='png|jpeg|jpeg|gif|bmp|pjpeg|pjpg',$allowSize=2048000)
{
    if(!isset($_FILES[$form_name]['name']) || $_FILES[$form_name]['size']<=0)
    {
        return array(1,'没有找到需要上传的文件');
    }
    $type = strtolower($_FILES[$form_name]['type']);
    $type = explode('/', $type);
    $type = $type[1];
    
    if(false === strpos($allowExt, $type))
    {
        return array(2,'不允许上传文件类型'.$type);
    }
    if($_FILES[$form_name]['size'] > $allowSize)
    {
        $k = $allowSize / 1024;
        if($k>1024)
        {
            $k = $k/1024;
            $k = $k.'M';
        }
        else
        {
            $k = $k .'K';
        }
        return array(3,'上传的文件太大，不能超过'.$k);
    }
    $filename = time().'_'.rand_string().'.'.$type;
    if(!file_exists($savepath))
    {
        @mkdir($savepath);
    }
    $file = $savepath.$filename;
    $result = @move_uploaded_file($_FILES[$form_name]['tmp_name'], $file);

    if(!$result)
    {
        return array(4,'移动文件失败，请确认目录存在并且有读写权限');
    }

    return array(0,$file);
}

//字符串截取
function str_cut($sourcestr,$cutlength,$suffix='...')
{
    $str_length = strlen($sourcestr);
    if($str_length <= $cutlength) {
        return $sourcestr;
    }
    $returnstr='';  
    $n = $i = $noc = 0;
    while($n < $str_length) {
            $t = ord($sourcestr[$n]);
            if($t == 9 || $t == 10 || (32 <= $t && $t <= 126)) {
                $i = 1; $n++; $noc++;
            } elseif(194 <= $t && $t <= 223) {
                $i = 2; $n += 2; $noc += 2;
            } elseif(224 <= $t && $t <= 239) {
                $i = 3; $n += 3; $noc += 2;
            } elseif(240 <= $t && $t <= 247) {
                $i = 4; $n += 4; $noc += 2;
            } elseif(248 <= $t && $t <= 251) {
                $i = 5; $n += 5; $noc += 2;
            } elseif($t == 252 || $t == 253) {
                $i = 6; $n += 6; $noc += 2;
            } else {
                $n++;
            }
            if($noc >= $cutlength) {
                break;
            }
    }
    if($noc > $cutlength) {
            $n -= $i;
    }
    $returnstr = substr($sourcestr, 0, $n);
 

    if ( substr($sourcestr, $n, 6)){
          $returnstr = $returnstr . $suffix;//超过长度时在尾处加上省略号
      }
    return $returnstr;
}
