<?php

function __process_curl__($ch,$json = true) {
	$ret = array ('code' => 0,'message'=>'', 'data'=>array(),'content' =>'','http_code'=>0,'header_size'=>0);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
	
	$ret['content'] = curl_exec($ch);
    $ret['code'] = curl_errno($ch);
    $ret['http_code'] = curl_getinfo($ch,CURLINFO_HTTP_CODE);
    $ret['header_size'] = curl_getinfo($ch,CURLINFO_HEADER_SIZE);    
    if($ret['code'] === 0)
    {
        if($json)
        {
            @$ret['data'] = json_decode($ret['content'],true);
            if(!is_array($ret['data']))
            {
                $ret['code'] = 10;
            }
        }
    }
    else
    {
        $ret['message'] = curl_error($ch);
    }

	curl_close($ch);

	return $ret;

}

function do_get($url, $data, $json = true)
{
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);

	$get_data = "";
	
	foreach($data as $k => $v){
		if (strlen($get_data) > 0)
		{
			$get_data .= '&';
		}

		$get_data .= $k . '=' . urlencode($v);
	}

	curl_setopt($ch, CURLOPT_URL, $url . '?' . $get_data);

	return __process_curl__($ch,$json);
}

function do_post($url, $data,$json = true) {
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
	curl_setopt($ch, CURLOPT_POST, TRUE);

	$post_data = "";
	
	foreach($data as $k => $v){
		if (strlen($post_data) > 0)
		{
			$post_data .= '&';
		}

		$post_data .= $k . '=' . urlencode($v);
	}

	curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);

	return __process_curl__($ch,$json);
}

