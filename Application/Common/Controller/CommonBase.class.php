<?php
/**
 * Created by IntelliJ IDEA.
 * User: appie
 * Date: 14-4-29
 * Time: 下午2:24
 */

namespace Common\Controller;

use Think\Controller;

class CommonBase extends Controller
{
    protected $WEB_NAME;
    protected $dao;

    function __construct()
    {
        parent::__construct();

        $this->WEB_NAME='WowCam';

        $this->assign('__ACTION_NAME__',ACTION_NAME);
        $this->assign('__MOUDLE_NAME__',CONTROLLER_NAME);
        $this->assign('__THEME_NAME__',THEME_NAME);
        $this->assign('__TMPL__',THEME_PATH);

        //echo '__construct,'.ACTION_NAME.','.MOUDLE_NAME.','.THEME_NAME.','.THEME_PATH;
    }

    protected function out($result)
    {
        $callback = I('callback');
        $msg = var_export($result,true);
        file_put_contents('1.txt', $msg."\n",FILE_APPEND);
        if($callback)
        {
            die($callback.'('.json_encode($result).')');
        }
        else
        {
            die(json_encode($result));
        }
    }

    protected function updateGiftNum($actid,$people = 0)
    {
        $actmod = M('activity');
        $row = $actmod->getById($actid);
        if(!is_array($row))
        {
            return;
        }
        if($people == 0)
        {
            $people = $row['people'];
        }
        
        $mod = M('gift');
        $giftcount = $mod->where('actid='.$actid.' and isAward=1')->sum('num');
        $notAwardNum = $mod->where('actid='.$actid.' and isAward=0')->count();
        //没有增加没有中奖的项目，直接退出
        if($notAwardNum == 0)
        {
            return;
        }
        //计算没有中奖的概率
        $value = ($people - $giftcount) / $notAwardNum;
        if($value<0)
        {
            return;
        }
        $data = array('num'=>$value);
        $mod->data($data)->where('actid='.$actid.' and isAward=0')->save();
    }
} 
