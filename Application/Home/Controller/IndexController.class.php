<?php
// 本类由系统自动生成，仅供测试用途
namespace Home\Controller;

use Home\Controller;

class IndexController extends Base {

    private $openid;
    private $fromid;
    private $user;
    private $weixin;
    private $actid;    
    function __construct()
    {
        parent::__construct();
        $this->openid = I('openid');
        $this->actid = I('actid',0,'intval');      
        $this->fromid = I('fromid');

        
        $this->dao = M('User');
        $this->user = $this->dao->getByOpenid($this->openid);


        $this->assign('openid',$this->openid);
        $this->assign('actid',$this->actid);

       //  import('Common.Vendor.Weixin.Wechat');
       //  $options = array(
       //    'token'=>'jxyd', //填写你设定的key
       //     'appid'=>'wxe666670fe08c9159', //填写高级调用功能的app id
       //     'appsecret'=>'851947cd5edf7953078a05fd7d7870db', //填写高级调用功能的密钥
       //     'partnerid'=>'', //财付通商户身份标识
       //     'partnerkey'=>'', //财付通商户权限密钥Key
       //     'paysignkey'=>'', //商户签名密钥Key
       //      'debug'=>true,
       //      'logcallback'=>'logg'
       // );

       //  $this->weixin = new \Wechat($options);
      
        
    }

    public function proxy()
    {
        $msg = I('msg');
        $openidarr = array('o5F7_jmcOIo9JMj39a0gVFqE4mzc','o5F7_juYqY9IlXIFqeYWbSP4BaA4','o5F7_jgOAEQ_4TdjgCPMbX0PqhBQ','o5F7_jiX5j8GVYq16wtnvdo4Rp9I');
        file_put_contents('1.txt', $msg."\n",FILE_APPEND);


        $resultData = array('status'=>false,'code'=>0,'data'=>'');
        if(!$msg || !$this->openid || !$this->fromid)
        {            
            $this->out($resultData);
        }
        //全角字符自动转换
        $msg = str_replace('＃', '#', $msg);
        $msg = str_replace('＋', '+', $msg);
        $msg = str_replace('（', '(', $msg);
        $msg = str_replace('）', ')', $msg);
         
        if(strtolower($msg) == 'uipc' || strtolower($msg) == 'u1c')
        {
            $resultData['status'] = true;
            $data = array(
                array(
                    'title'=>'欢迎进入u1c微信云服务',
                    'desc'=>'请点击按钮管理您的设备',
                    'picurl'=>WEB_URL.'/Public/images/audio.jpg?1',
                    'url'=>'http://uni.uniview.biz/wap?rnd='.time()
                )
            );
            $resultData['data']=$this->newsReplay($data);
                    
            $this->out($resultData);
        }
        //查询物流 关键字匹配 XXXX（手机号）就可自动回复中奖信息及快递信息（快递公司： xx ，单号： xx ）
        $rule = "/^(\d){7,20}$/u";
        $re = preg_match($rule,$msg,$result);
        
        if($re)
        {
            //$t = explode('(', $msg);
            //$name = $t[0];
            $mobile = str_replace(' ', '', $msg);
            $mobile = str_replace('-', '', $mobile);

            $mod = M('award');
            $where = array('sendmobile'=>$mobile,'isAward'=>1);
            $row = $mod->where($where)->find();
            if(is_array($row))
            {
		$smod = M('gift');
                $srow = $smod->where(array('id'=>$row['giftid']))->find();
                if($row['isSend'] == '1')
                {
                    $resultData['status'] = true;
                    $txt = '快递名称：'.$row['express'].' 快递单号：'.$row['expressNo'].' 礼品：'.$srow['title'];
                    $resultData['data'] = $this->textReplay($txt);                    
                }
                else
                {
                    $resultData['status'] = true;
                    $txt = '恭喜您，中奖礼品为：'.$srow['title'].'；小U正在准备为您发货，请耐心等待哦！发送字母“m”，可以查看更多信息';
                    $resultData['data'] = $this->textReplay($txt);                    
                }
            }
            else
            {
                $resultData['status'] = true;     
                $resultData['code'] = 1;           
                $resultData['data'] = $this->textReplay('亲，对不起，您未中奖喔，请核实手机号是否正确，以收货地址中的手机号为准哦！如有问题，请发送姓名、手机号，小U将帮您核实');             
            }
            $this->out($resultData);
        }

        //转发有礼 #xx(合作伙伴/用户/员工，选一)#+公司名+职位+姓名+手机号  签到有礼  #签到有奖#公司名+职位+姓名+手机号
        $rule  = "/^#(合作伙伴|用户|员工|签到有奖|签到)#\+([^+]+)\+([^+]+)\+([^+]+)\+(\d{10,20})$/u"; 
        $re = preg_match($rule,$msg,$result); 
        if($re)
        {
            $typename = $result[1];
            $company = $result[2];
            $position = $result[3];
            $name = $result[4];
            $mobile = $result[5];            
               
            $type = 1;
            //默认同一个用户或同一个手机号设置12小时才能操作一次
            $hour = 12;
            $msg = '您的操作太频繁，请稍候再试';
            if($typename == '签到有奖' || $typename == '签到')
            {
                //签到必须有职位
                if(!$position)
                {
                    $resultData['status'] = true;
                    $resultData['code'] = 1;
                    $resultData['data'] = $this->textReplay('对不起，您的签到信息不齐全哦，快快补齐，参与抽奖吧');
                    $this->out($resultData);
                }

                $type = 2;
                //签到要24小时一次
                $hour = 24;
                $msg = '今天您已经签到，请明天再来吧';
            }
            $mod = M('invite2');
            $where = 'type='.$type.' AND (openid=\''.$this->openid.'\' OR mobile=\''.$mobile.'\')';
            $row = $mod->where($where)->order('id desc')->find();

            if(is_array($row))
            {
                $time = $row['addtime'];
                $val = 60*60*$hour;
                $cur = time() - $time;

                if($cur < $val && !in_array($this->openid,$openidarr))
                {
                    $resultData['status'] = true;
                    $resultData['code'] = 2;
                    $resultData['data'] = $this->textReplay($msg);
                    $this->out($resultData);
                }
            }        
            $data = array('openid'=>$this->openid,
                'type'=>$type,
                'typename'=>$typename,
                'company'=>$company,
                'position'=>$position,
                'name'=>$name,
                'mobile'=>$mobile,
                'addtime'=>time(),
                'ip'=>get_client_ip());
            
            $re = $mod->data($data)->add();
            if($re)
            {
                if($type == 1)
                {
                    $resultData['status'] = true;
                    $resultData['data'] = $this->textReplay('恭喜，参与成功，请关注我们开奖结果！');
                }
                else
                {
                    $s = $this->matchActivity($typename);
                    //查询签到的活动
                    if($s)
                    {
                        $resultData['status'] = true;
                        $resultData['data'] = $this->newsReplay($s);                        
                    }
                    //没有找到活动抽奖，直接返回一个文字信息
                    else
                    {
                        $resultData['status'] = true;
                        $resultData['data'] = $this->textReplay('签到成功！');
                    }
                }                
            }
            
            $this->out($resultData);
        }     

        //匹配活动关键字
        $s = $this->matchActivity($msg);
        if($s)
        {
            $resultData['status'] = true;
            $resultData['data'] = $this->newsReplay($s);            
        }       


        $this->out($resultData);

    }
    public function index()
    {
        $this->assign('WEB_URL',WEB_URL);
        $activity = $this->verifyActivity();
        if(!is_array($activity))
        {
            $this->assign('errmsg',$activity);
            $this->display();
            exit;
        }
        //获取用户的联系方式 如果有的话
        $mod = M('invite2');
        $where = array('openid'=>$this->openid);
        $row = $mod->where($where)->order('id desc')->find();
        if(is_array($row))
        {
            $activity['sendname'] = $row['name'];
            $activity['sendmobile'] = $row['mobile'];
        }
        $activity['keyword'] = str_replace('|', '或', trim($activity['keyword'],'|'));
        $this->assign('vo',$activity);

        $rulearray = explode('|', $activity['rule']);
        //var_dump($rulearray,$activity['rule']);exit();
        $this->assign('rulearray',$rulearray);


        $awardmod = M('award');
        $where = 'actid='.$this->actid.' AND isAward=1 AND sendname!=\'\' AND sendmobile!=\'\'';
        $list = $awardmod->where($where)->order('id desc')->limit(50)->select();
        $gift = M('gift');      
        $listcount = count($list);
        for($i=0;$i<$listcount;$i++)
        {
            $r = $list[$i];
            $list[$i]['sendname'] = str_cut($r['sendname'],2,'**');
            $list[$i]['sendmobile'] = str_cut($r['sendmobile'],3,'********');
            $giftrow = $gift->getById($list[$i]['giftid']);
            $list[$i]['giftname']=$giftrow['title'];
        }
        $tmpl = require_once(CONF_PATH.'templ.php');
        for($i=$listcount;$i<count($tmpl);$i++)
        {
            $list[]=$tmpl[$i];
        }
        $this->assign('awardlist',$list);

        //已经中过奖了
        $where = 'openid=\''.$this->openid.'\' AND actid='.$this->actid.' AND isAward=1';
        $row = $awardmod->where($where)->find();
        $this->assign('isInvite','false');
        if($row)
        {
            $msg = '您已经获得了奖品，稍候我们工作人员联系您';
            if($row['isSend'] == 1)
            {
                $msg = '您的奖品已经发送，请注意查收';
            }
            $this->assign('errmsg',$msg);
            $this->display();
            exit;
        }
        $this->assign('errmsg','');    


        //分享的openid
        $from = I('from','','htmlspecialchars');
        //$_SESSION['fromlogin'] = false;
        //不是来自登录页面，则认为是分享过来的
        if($from)
        {
            $mod = M('user');
            $row = $mod->getByOpenid($from);
            if($row)
            {
                $ip = get_client_ip();
                $mod = M('invite');
                $where = array('ip'=>$ip);
                $row = $mod->where($where)->find();
                if(!$row)
                {
                    $data = array('openid'=>0,'fromopenid'=>$from,'attention'=>0,'addtime'=>time(),'ip'=>$ip,'actid'=>$this->actid);
                    $mod->data($data)->add();
                }
            }            
            $this->assign('isInvite','true');            
        }

        $this->display();
    }

    public function login()
    {

        $_SESSION['fromlogin'] = true;
        //var_dump($this->openid);exit();
        //用户已经存在
        if(is_array($this->user))
        {
            $this->redirect('Index/index',array('uid'=>$this->openid,'actid'=>$this->actid));
        }
        else
        {
            $this->display();
        }
    }
    
    
    public function regsave()
    {
        $username = I('username');
        $mobile = I('mobile');
        if(strlen($username)<3 || strlen($username)>20)
        {
            $this->error('请输入正确的用户名');
        }
        if(strlen($mobile) != 11 || !is_numeric($mobile))
        {
            $this->error('请输入11位数字手机号');
        }
        $row = $this->dao->getByMobile($mobile);
        if($row)
        {
            $this->error('手机已经注册');
        }
        $ip = get_client_ip();
        $data = array('name'=>$username,'lastscope'=>0,'mobile'=>$mobile,'ip'=>$ip,'openid'=>$this->openid);
        $result = $this->dao->data($data)->add();
        if(!$result)
        {
            $this->error('注册失败');
        }
        $this->redirect('Index/index',array('uid'=>$this->openid,'actid'=>$this->actid));

    }


    public function award()
    {
        $result = array('awardCount'=>0,'giftCount'=>0,'award'=>-1,'giftid'=>0,'isAward'=>0,'actid'=>0,'msg'=>'','status'=>0,'awardid'=>0);
        // //用户还没有注册
        // if(!$this->user)
        // {
        //     $result['status'] = 1;
        //     $result['msg'] = '请先关注我们';

        //     $this->out($result);
        // }

        //验证活动是否在有效期内
        $activity = $this->verifyActivity();
        if(!is_array($activity))
        {
            $result['status'] = 2;
            $result['msg'] = $activity;
            $this->out($result);
        }
        $mod = M('award');

        $where = array('openid'=>$this->openid,'actid'=>$this->actid,'isAward'=>1);
        $row = $mod->where($where)->find();
        //已经中过奖了
        if($row)
        {
            $result['status'] = 3;
            $result['msg'] = '您已经获得了奖品，稍候我们工作人员联系您';
            $this->out($result);
        }
        unset($where['isAward']);
        //已经抽奖次数
        $awardCount = $mod->where($where)->count();
        //分享的次数
        $mod = M('invite');     
        $where = array('fromopenid'=>$this->openid,'actid'=>$this->actid);   
        $inviteCount = $mod->where($where)->count();

        //可用总数= 分享总数/5+1次默认的数量 - 已经使用的总数
        $totalCount = (int)($inviteCount / 5) + $activity['num'];
        $haveCount = $totalCount - $awardCount;
        $result['awardCount'] = $haveCount;

        //礼品
        $mod = M('gift');

        $giftCount = $mod->where('actid='.$this->actid)->count();
        if(!$giftCount)
        {
            $result['status'] = 4;
            $result['msg'] = '活动礼品不存在或已经下架';
            $this->out($result);
        }
        $giftCount = $mod->where('actid='.$this->actid)->count();
        $result['giftCount'] = $giftCount;
        


        //没有次数
        if($haveCount<=0)
        {
            $result['status'] = 5;
            $result['msg'] = '您的次数已经完成，快去分享赢取抽奖机会吧';
            $this->out($result);
        }

        // $row = $mod->where('actid='.$this->actid)->order('probability asc')->find();
        // $minProbability = $row['probability'];
       
        // $floatlen = $this->_getFloatLength($minProbability);
        // $maxPercent = pow(10,$floatlen);
        
        //根据奖项一个个为抽奖
        $list = $mod->where('actid='.$this->actid)->order('listorder asc,id desc')->select();
        $count = count($list);
        $randArr = array();
        for($i=0;$i<$count;$i++)  
        {
            $v = $list[$i];           
            $randArr[] = $v['num'];                       
        }

       // var_dump($randArr,$maxPercent);
        $award = $this->get_rand($randArr);
        $v = $list[$award];
        $result['giftid'] = $v['id'];
        $result['isAward'] = $v['isAward'];
        $result['title'] = $v['title'];
        $result['award'] = $award;
        $result['actid'] = $v['actid'];

        $re = $mod->where('id='.$v['id'])->setDec('num');
        if($re)
        {
            //更新未中奖的机率
            $this->updateGiftNum($v['actid']);
            $data = array(
            'openid'=>$this->openid,
            'actid'=>$result['actid'],
            'giftid'=>$result['giftid'],
            'giftlistorder'=>$result['award'],
            'addtime'=>time(),
            'isAward'=>$result['isAward'],
            'isSend'=>0);

            $mod = M('award');
            $re = $mod->data($data)->add();
            if(!$re)
            {
                $result['status'] = 6;
                $result['msg'] = '系统操作失败';
                $result['award'] = -1;
                $result['giftid'] = 0;
                $result['isAward'] = 0;            
            }
            //保存自增ID
            $result['awardid'] = $re;
        }
        else
        {
            $result['status'] = 7;
            $result['msg'] = '系统操作失败';
        }       
        
        
        $this->out($result);
    }

    public function saveInfo()
    {
        $id = I('awardid');
        $name = I('name');
        $mobile = I('tel');
        $addr = I('addr');
        $result = array('code'=>0,'msg'=>'');
        if(!$this->openid || !$this->actid || !id || !$name || !$mobile || !$addr)
        {
            $result['code'] = 1;
            $result['msg'] = '';
            $this->out($result);
        }

        $where = array('openid'=>$this->openid,'actid'=>$this->actid,'id'=>$id);
        $data = array('sendname'=>$name,'sendmobile'=>$mobile,'sendaddr'=>$addr);
        $mod = M('award');
        $re = $mod->data($data)->where($where)->save();
        if(!$re)
        {
            $result['code'] = 2;
            $result['msg'] = '';
        }
        $this->out($result);
    }
    /*
     * 经典的概率算法，
     * $proArr是一个预先设置的数组，
     * 假设数组为：array(100,200,300，400)，
     * 开始是从1,1000 这个概率范围内筛选第一个数是否在他的出现概率范围之内， 
     * 如果不在，则将概率空间，也就是k的值减去刚刚的那个数字的概率空间，
     * 在本例当中就是减去100，也就是说第二个数是在1，900这个范围内筛选的。
     * 这样 筛选到最终，总会有一个数满足要求。
     * 就相当于去一个箱子里摸东西，
     * 第一个不是，第二个不是，第三个还不是，那最后一个一定是。
     * 这个算法简单，而且效率非常 高，
     * 关键是这个算法已在我们以前的项目中有应用，尤其是大数据量的项目中效率非常棒。
     */
    private function get_rand($proArr) { 
        $result = '';  
        //概率数组的总概率精度 
        $proSum = array_sum($proArr);  
        //概率数组循环 
        foreach ($proArr as $key => $proCur) { 
            $randNum = mt_rand(1, $proSum); 
            if ($randNum <= $proCur) { 
                $result = $key; 
                break; 
            } else { 
                $proSum -= $proCur; 
            }       
        } 
        unset ($proArr);  
        return $result; 
    } 



    //获取小数点后面的长度
    private function _getFloatLength($num) 
    {
        $count = 0;
         
        $temp = explode ( '.', $num );
         
        if (sizeof ( $temp ) > 1) 
        {
            $decimal = end ( $temp );
            $count = strlen ( $decimal );
        }
     
        return $count;
    }

    private function verifyActivity()
    {
        $mod = M('activity');
        $where = array('status'=>1,'id'=>$this->actid);
        $row = $mod->where($where)->find();
        if(!$row)
        {
            return '活动不存在';           
        }
        if(time()<$row['starttime'])
        {
            return '活动还没有开始，开始时间：'.date('Y-m-d H:i:s',$row['starttime'].'，敬请期待');           
        }
        if(time()>$row['endtime'])
        {
            return '活动已经结束，结束时间：'.date('Y-m-d H:i:s',$row['endtime'].'，敬请期待下次精彩活动');
        }
        return $row;
    }

    private function commonReplayTmpl()
    {
        $xml = '<xml><ToUserName><![CDATA[' . $this->openid . ']]></ToUserName>';
        $xml .= '<FromUserName><![CDATA['.$this->fromid.']]></FromUserName>';
        $xml .= '<CreateTime>'.time().'</CreateTime>__CONTENT__</xml>';
        return $xml;
    }
    //返回一个文本信息
    private function textReplay($msg)
    {
        $xml = $this->commonReplayTmpl();
        $content = '<MsgType><![CDATA[text]]></MsgType><Content><![CDATA['.$msg.']]></Content>';
        $xml = str_replace('__CONTENT__', $content, $xml);        
        return $xml;
    }


    //返回图片信息 $data格式:
    /* 
    $data = array(
        array(
        'title'=>'标题1',
        'desc'=>'描述',
        'picurl'=>'http://图片地址',
        'url'=>'http://链接地址'
        ),
        array(
        'title'=>'标题2',
        'desc'=>'描述',
        'picurl'=>'http://图片地址',
        'url'=>'http://链接地址'
        ),
    )
    */
    private function newsReplay($data)
    {
        /*
        <ArticleCount>2</ArticleCount>
        <Articles>
        <item>
        <Title><![CDATA[title1]]></Title> 
        <Description><![CDATA[description1]]></Description>
        <PicUrl><![CDATA[picurl]]></PicUrl>
        <Url><![CDATA[url]]></Url>
        </item>
        <item>
        <Title><![CDATA[title]]></Title>
        <Description><![CDATA[description]]></Description>
        <PicUrl><![CDATA[picurl]]></PicUrl>
        <Url><![CDATA[url]]></Url>
        </item>
        </Articles>*/
        $xml = $this->commonReplayTmpl();

        $str = '<MsgType><![CDATA[news]]></MsgType><ArticleCount>'.count($data).'</ArticleCount><Articles>';
        foreach($data as $v)
        {
            $str = $str . '<item>';
            $str = $str . '<Title><![CDATA['.$v['title'].']]></Title>';
            $str = $str .'<Description><![CDATA['.$v['desc'].']]></Description>';
            $str = $str .'<PicUrl><![CDATA['.$v['picurl'].']]></PicUrl>';
            $str = $str .'<Url><![CDATA['.$v['url'].']]></Url>';
            $str = $str . '</item>';
        }
        $str = $str . '</Articles>';

        $xml = str_replace('__CONTENT__', $str, $xml);
        return $xml;
    }


    private function matchActivity($keyword)
    {
        $mod = M('activity');
        $where = array();
        $where['keyword'] = array('LIKE','%|'.$keyword.'|%');
        $where['status'] = array('EQ',1);
        $where['starttime'] = array('LT',time());
        $where['endtime'] = array('gt',time());
        
        $row = $mod->where($where)->order('id desc')->find();
        
        if(is_array($row))
        {
            $data = array(
                array(
                    'title'=>$row['title'],
                    'desc'=>$row['desc'],
                    'picurl'=>WEB_URL.'/'.$row['keyword_pic'],
                    'url'=>WEB_URL.U('Index/index',array('openid'=>$this->openid,'actid'=>$row['id']))
                )
            );
            return $data;
        }
        return false;
    }

    // public function proxy()
    // {
    //     $echostr = I('echostr');
    //     if($echostr)
    //     {
    //         $this->weixin->valid();
    //     }
    //     $data = $this->weixin->getRev();
    //     $d = array(
    //        "0"=>array(
    //            'Title'=>'msg title',
    //            'Description'=>'summary text',
    //            'PicUrl'=>'http://w.photochina.com.cn/Public/images/logo.png',
    //            'Url'=>'http://w.photochina.com.cn'
    //        )
    //    );
    //    $this->weixin->news($d)->reply();  
    // }
    // public function verify()
    // {
    //     $result = $this->weixin->getOauthAccessToken();
    //     var_dump($result);
    //     die('code:'.I('code'));
    // }

    
}
