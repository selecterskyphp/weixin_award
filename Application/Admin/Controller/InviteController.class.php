<?php
// 本类由系统自动生成，仅供测试用途
namespace Admin\Controller;



class InviteController extends Base {

    private $actid;

    function __construct()
    {
        parent::__construct();        
        
        $this->dao = M('invite');  

        $this->assign('INVITE_ON',' class="active"');
        $this->assign('TITLE','分享列表'.$this->title); 
    }
	public function index()
    {
        $openid = I('openid');
        $where = array();
        if($openid)
        {
            $where['fromopenid'] = $openid;
        }
        $list = $this->dao->where($where)->order('addtime desc')->select();
        $user = M('User');	
        for($i=0;$i<count($list);$i++)
        {
            $row = $user->getByOpenid($list[$i]['fromopenid']);
            $list[$i]['username'] = $row['name'];
            $list[$i]['mobile'] = $row['mobile'];            
        }
        $this->assign('list',$list);
        
        $this->display();
    }
    
}
