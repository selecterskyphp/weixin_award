<?php
// 本类由系统自动生成，仅供测试用途
namespace Admin\Controller;


class AwardController extends Base {

    private $actid;
    private $where;
    function __construct()
    {
        parent::__construct();        
        
        $this->dao = M('award');      
        $user = I('user');
        $mobile = I('mobile');
        $award = I('award');
        $actid = I('actid');

        $this->where = 'openid!=\'\'';
        if($user)
        {
            $this->where = $this->where .'  AND sendname LIKE \'%'.$user.'%\'';           
        }
        if($mobile)
        {
            $this->where = $this->where . ' AND sendmobile LIKE \'%'.$mobile.'%\'';
        }
        if($award)
        {
            $this->where = $this->where . ' AND isAward=1';
        }
        if($actid)
        {
            $this->where = $this->where . ' AND actid='.$actid;
        }
        $this->assign('user',$user);
        $this->assign('mobile',$mobile);
        $this->assign('award',$award);
        $this->assign('actid',$actid);

        $this->assign('AWARD_ON',' class="active"');
        $this->assign('TITLE','抽奖记录'.$this->title); 
    }

    public function index()
    {
        $data = $this->getList();
        $list = $data['list'];
        $count = count($list);
        $awardCount = $data['awardCount'];
        $totalInviteCount = $data['totalInviteCount'];
        $pre = intval(floatval($awardCount / $count) * 100);
        $body = $body . "\r\n" . iconv('utf-8', 'gb2312', '总中奖的总数：').$awardCount.iconv('utf-8', 'gb2312', '，中奖机率：').$pre.iconv('utf-8', 'gb2312','%，总分享次数：').$totalInviteCount;

        $this->assign('list',$list);
        $this->assign('awardCount',$awardCount);
        $this->assign('awardParent',$pre);
        $this->assign('totalInviteCount',$totalInviteCount);
        $this->assign('count',$count);
        
        $this->display();
    }

    public function export()
    {
        $data = $this->getList();        
        $body .= iconv('utf-8','gb2312','活动主题');
        $body .= ','.iconv('utf-8','gb2312','奖品');
        $body .= ','.iconv('utf-8','gb2312','抽奖时间');  
        $body .= ','.iconv('utf-8','gb2312','是否中奖');
        $body .= ','.iconv('utf-8','gb2312','收货姓名');
        $body .= ','.iconv('utf-8','gb2312','手机');
        $body .= ','.iconv('utf-8','gb2312','收货地址');
        $body .= ','.iconv('utf-8','gb2312','发货时间');
        $body .= ','.iconv('utf-8','gb2312','快递名称');
        $body .= ','.iconv('utf-8','gb2312','快递单号');
        $list = $data['list'];
        foreach ($list as $vo)
        {
            $isAward = ($vo['isAward'] == 1)?'是':'否';
            $body .= "\r\n";
            $body .= iconv('utf-8','gb2312',$vo['acttitle']);
            $body .= ','.iconv('utf-8','gb2312',$vo['gifttitle']);
            $body .= ','.date('Y-m-d H:i:s',$vo['addtime']);
            $body .= ','.iconv('utf-8','gb2312',$isAward);
            $body .= ','.iconv('utf-8','gb2312',$vo['sendname']);
            $body .= ','.$vo['sendmobile'];
            $body .= ','.iconv('utf-8','gb2312',$vo['sendaddr']);
            $body .= ','.date('Y-m-d H:i:s',$vo['sendtime']);
            $body .= ','.iconv('utf-8','gb2312',$vo['express']);
            $body .= ','.$vo['expressNo'];
        }
        $count = count($list);
        $awardCount = $data['awardCount'];
        $totalInviteCount = $data['totalInviteCount'];
        $pre = intval(floatval($awardCount / $count) * 100);
        $body .= "\r\n";
        $body .= iconv('utf-8', 'gb2312', '参与人数：').$count.iconv('utf-8', 'gb2312', '，中奖人数：').$awardCount.iconv('utf-8', 'gb2312', '，中奖机率：').$pre.iconv('utf-8', 'gb2312','%，总分享次数：').$totalInviteCount;
        $path = './Uploads/export.csv';
        $result = 0;
        @$result = file_put_contents($path, $body);
        if($result)
        {
            $f = fopen($path,"r"); // 打开文件
            // 输入文件标签
            Header("Content-type: application/octet-stream");
            Header("Accept-Ranges: bytes");
            Header("Accept-Length: ".filesize($path));
            Header("Content-Disposition: attachment; filename=export.csv");
            // 输出文件内容
            echo fread($f,filesize($path));
            fclose($f);
        }
        else
        {
            $this->error('导出失败，请确认目录有写入权限');
        }

    }

    public function send()
    {
        $id = I('id');
        $this->assign('vo',$this->dao->getById($id));
        $this->display();
    }

    public function save()
    {
        $_POST['sendtime'] = strtotime($_POST['sendtime']);
        parent::save();
    }


    private function getList()
    {
        $list = $this->dao->where($this->where)->select();
        //var_dump($list);exit();
        $invite = M('invite');  
        $act = M('Activity'); 
        $gift = M('gift');
        //总中奖的总数
        $awardCount = 0;
        //总分享的总数
        $totalInviteCount = 0;
        for($i=0;$i<count($list);$i++)
        {
            $count = $invite->where('fromopenid='.$list[$i]['openid'])->count();
            if(!$count)
            {
                $count = 0;
            }
            $list[$i]['inviteCount'] = $count;
            $row = $act->getById($list[$i]['actid']);
            $list[$i]['acttitle'] = $row['title'];
            if($list[$i]['isAward'] == '1')
            {
                $awardCount++;
            }
            $totalInviteCount += $count;
            $row = $gift->getById($list[$i]['giftid']);
            $list[$i]['gifttitle'] = $row['title'];
        }
        return array('awardCount'=>$awardCount,'totalInviteCount'=>$totalInviteCount,'list'=>$list);
    }
    
}
