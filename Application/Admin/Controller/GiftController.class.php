<?php
// 本类由系统自动生成，仅供测试用途
namespace Admin\Controller;



class GiftController extends Base {

    private $actid;
    private $people;
    function __construct()
    {
        parent::__construct();        
        
        $this->dao = M('gift');
        $this->actid = I('actid',0);
        $this->checkActId();
        $this->assign('actid',$this->actid);   
        $this->assign('HOME_ON',' class="active"');
    }

    public function index()
    {
        $this->assign('TITLE','礼品列表'.$this->title); 
        $list = $this->dao->where('actid='.$this->actid)->order('listorder asc,id desc')->select();
        for($i=0;$i<count($list);$i++)
        {
            $num = $list[$i]['num'];
            $parent = floatval($num) / floatval($this->people);
            //保留4位小数
            $parent = sprintf('%.4f',$parent);           
            $list[$i]['probability'] = 100*$parent;
        }
        $this->assign('list',$list);
        
        $this->display();
    }
    
    public function save()
    {   
        // if(!isset($_POST['id']))
        // {
           
        //     $_POST['addtime'] =time();
        //     $_POST['hits'] = 0;
        //     $_POST['listorder'] = 0;
        // }
    

        if(!$this->dao->create($_POST))
        {
            $this->error($this->dao->getError());
        }
        $id = I('id',0,'intval');        
        if($id>0)
        {
            $result =   $this->dao->save();
        }
        else
        {
            $result =   $this->dao->add();
        }
        $this->updateGiftNum($this->actid);
        if(false !== $result) 
        {         
            $this->assign('jumpUrl',U(CONTROLLER_NAME.'/index',array('actid'=>$this->actid)));
            $this->success('操作成功');
        }else{
            
            $this->error('操作失败');
        }
    }

    public function del()
    {
       $id = I('id',0,'intval');
       $where = array('id'=>$id);
       $this->dao->where($where)->delete();
       $this->updateGiftNum($this->actid);
       $this->redirect(CONTROLLER_NAME.'/index',array('actid'=>$this->actid));
    }


    private function checkActId()
    {
        $mod = M('activity');
        $row = $mod->getById($this->actid);
        if(!$row)
        {
            $this->error('活动不存在');
        }
        $this->people = $row['people'];
    }

}
