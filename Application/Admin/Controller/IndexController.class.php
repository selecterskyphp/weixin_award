<?php
// 本类由系统自动生成，仅供测试用途
namespace Admin\Controller;



class IndexController extends Base {

    function __construct()
    {
        parent::__construct();        
        
        $this->dao = M('activity');
        $this->assign('HOME_ON',' class="active"');
    }

    public function index()
    {
        $this->assign('HOME_ON',' class="active"');
        $this->assign('TITLE','首页'.$this->title); 
        parent::index();
    }
    
    public  function login()
    {
        $this->display();
    }

    public  function edit()
    {
        $id = I('get.id',0,'intval');
        if($id>0)
        {   
            $vo = $this->dao->getById($id);           
        }
        else
        {
            $vo = array('starttime'=>time(),'endtime'=>time()+60*60*24*30,'num'=>1);
        }
        
        $this->assign('vo',$vo);
        $this->assign('id',$id);
        $this->display();
    }

    public function save()
    {
        $_POST['starttime'] = strtotime($_POST['starttime']);
        $_POST['endtime'] = strtotime($_POST['endtime']);
        $vo = array();
        $id = I('id');
        $people = I('people',0,'intval');
        if($id)
        {
            $mod = M('activity');
            $vo = $mod->getById($id);            
        }
        $result = upload_file('banner');
        if($result[0] === 0)
        {
            if($vo['banner'])
            {
                @unlink($vo['banner']);
            }
            
            $_POST['banner'] = $result[1];            
        }
        //不是修改状态 或者设置了文件但是上传失败了 就提示
        else if(!$id || $result[0] != 1)
        {
            //$this->error('上传banner失败：'.$result[1]);
        }
        

        $result = upload_file('award_bg');



        if($result[0] === 0)
        {
            if($vo['award_bg'])
            {
                @unlink($vo['award_bg']);
            }
            $_POST['award_bg'] = $result[1];            
        }
        else if(!$id || $result[0] != 1)
        {
            $this->error('上传奖品图失败：'.$result[1]);
        }
        

        $result = upload_file('keyword_pic');
        if($result[0] === 0)
        {
            if($vo['keyword_pic'])
            {
                @unlink($vo['keyword_pic']);
            }
            $_POST['keyword_pic'] = $result[1];            
        }
        else if(!$id || $result[0] != 1)
        {
            $this->error('上传关键字图失败：'.$result[1]);
        }

        $result = upload_file('invite_logo');
        if($result[0] === 0)
        {
            if($vo['invite_logo'])
            {
                @unlink($vo['invite_logo']);
            }
            $_POST['invite_logo'] = $result[1];
            
        }
        else if(!$id || $result[0] != 1)
        {
            $this->error('上传分享LOGO图失败：'.$result[1]);
        }
        
        $this->updateGiftNum($id,$people);
        parent::save();
    }

    public function doLogin()
    {
        $username = I('post.name');
        $pwd = I('post.pwd');
        if(!$username || !$pwd)
        {
            $this->error('请输入正确登录信息');
        }
        $result = $this->wowcam->login($username,md5($pwd));
        if($result['code'] != 0)
        {
            //var_dump($result);
            $this->error($result['message']);
        }
        
        $this->assign('jumpUrl',U('Index/index'));
        $this->success('登录成功！');
    }

    public function logout()
    {
        $this->wowcam->logout();
        $mod = M('Token');
        $data = array('updatetime'=>0);
        $where = array();
        $where['token'] = array('neq','');
        $result = $mod->where($where)->data($data)->save();
        
        $this->redirect('Index/login');
    }
}
